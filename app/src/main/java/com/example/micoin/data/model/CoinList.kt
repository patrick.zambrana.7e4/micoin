package com.example.micoin.data.model

data class CoinList(
    var coins: List<Coin>
)