package com.example.micoin.data.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Coin::class), version = 1)
abstract class CoinDatabase: RoomDatabase() {
    abstract fun CoinDAO(): CoinDAO
}