package com.example.micoin.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Coins")
data class Coin(
    @PrimaryKey(autoGenerate = true) var idCoin: Long?,
    var isFavorite: Boolean = false,
    val id: String,
    val icon: String,
    val name: String,
    val symbol: String,
    val rank: Int,
    val price: Double,
    var priceBtc: Double?,
    var volume: Double?, //?
    //var marketCap: Int?, //? NOT WORKING
    var priceChange1h: Double?,
    var priceChange1d: Double?,
    val priceChange1w: Double,
    val websiteUrl: String,
    val twitterUrl: String?


    //val exp: List<String>
    //var availableSupply: Int,   //?

)

// API: https://documenter.getpostman.com/view/5734027/RzZ6Hzr3?version=latest