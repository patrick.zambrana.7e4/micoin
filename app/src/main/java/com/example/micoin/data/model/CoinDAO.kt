package com.example.micoin.data.model

import androidx.room.*

@Dao
interface CoinDAO {

    @Query("SELECT * FROM Coins")
    fun getAllCoins(): MutableList<Coin>
    @Insert
    fun addCoin(coin: Coin)
    @Update
    fun updateCoin(coin: Coin)
    @Delete
    fun deleteCoin(coin: Coin)
}