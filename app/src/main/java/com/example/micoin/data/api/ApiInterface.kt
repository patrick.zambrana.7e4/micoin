package com.example.micoin.data.api

import com.example.micoin.data.model.Coin
import com.example.micoin.data.model.CoinList
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("coins")
    fun getCoins(): Call<CoinList>

    @GET("coins/{id}")
    fun getCoin(@Path("id") idCoin: Long, @Body coin: Coin?): Call<Coin>

    companion object {
        private const val BASE_URL = "https://api.coinstats.app/public/v1/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}