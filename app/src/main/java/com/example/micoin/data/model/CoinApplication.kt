package com.example.micoin.data.model

import android.app.Application
import androidx.room.Room

class CoinApplication : Application() {
    companion object {
        lateinit var database:CoinDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
        CoinDatabase::class.java,
        "CoinDatabase").build()
    }
}