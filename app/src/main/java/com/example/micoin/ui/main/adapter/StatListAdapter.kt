package com.example.micoin.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.micoin.R
import com.example.micoin.data.model.Stat

class StatListAdapter() : RecyclerView.Adapter<StatListAdapter.StatListViewHolder>() {
    private var stats = mutableListOf<Stat>()

    fun setStats(statsList: List<Stat>) {
        stats = statsList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatListAdapter.StatListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_stat_list, parent, false)
        return StatListViewHolder(view)
    }

    override fun onBindViewHolder(holder: StatListAdapter.StatListViewHolder, position: Int) {
        holder.bindData(stats[position])
    }

    override fun getItemCount(): Int {
        //coins = viewModel.coinsAPI.value!!.toMutableList()
        return stats.size
    }

    inner class StatListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var statView : TextView = itemView.findViewById(R.id._stat_)

        fun bindData(stat: Stat) {
            statView.text = String.format("%s%s", stat.name, stat.value)
        }
    }
}