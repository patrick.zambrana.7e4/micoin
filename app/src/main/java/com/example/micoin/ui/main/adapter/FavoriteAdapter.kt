package com.example.micoin.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.micoin.MainActivity
import com.example.micoin.R
import com.example.micoin.data.model.Coin
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import com.squareup.picasso.Picasso

class FavoriteAdapter() : RecyclerView.Adapter<FavoriteAdapter.FavoriteListViewHolder>() {
    private var favCoins = mutableListOf<Coin>()

    fun setCoins(coins: List<Coin>) {
        favCoins = coins.toMutableList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteAdapter.FavoriteListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_coin_list, parent, false)
        return FavoriteListViewHolder(view)
    }

    override fun onBindViewHolder(holder: FavoriteAdapter.FavoriteListViewHolder, position: Int) {
        holder.bindData(favCoins[position])
    }

    override fun getItemCount(): Int {
        return favCoins.size
    }

    inner class FavoriteListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var coinName: TextView = itemView.findViewById(R.id.coinName)
        private var coinLogo: ImageView = itemView.findViewById(R.id.coinLogo)
        private var status: ImageView = itemView.findViewById(R.id.status)
        private var itemBackground: View = itemView.findViewById(R.id.item)
        val inflater: LayoutInflater = itemView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.coin_list_fragment, null)

        fun bindData(coin: Coin) {
            coinName.text = coin.name
            Picasso.get().load(coin.icon).resize(200, 200).centerCrop().into(coinLogo)
            if (coin.priceChange1w >= 0) {
                status.setImageResource(R.drawable.neon_arrow_up)
            } else {
                status.setImageResource(R.drawable.neon_arrow_down)
            }
            //coinLogo.background = ContextCompat.getDrawable(itemView.context, R.drawable.bitcoin)
            if (favCoins.indexOf(coin) % 2 != 0) {
                itemBackground.background = ContextCompat.getDrawable(itemView.context, R.color.topbar_background)
                coinName.setTextColor(ContextCompat.getColor(itemView.context, R.color.background_light_yellow))
            } else {
                itemBackground.background = ContextCompat.getDrawable(itemView.context, R.color.gray)
                coinName.setTextColor(ContextCompat.getColor(itemView.context, R.color.textLight))
            }

            itemBackground.setOnClickListener {
                (itemView.context as MainActivity).supportFragmentManager.commit() {
                    Navigation.findNavController(itemView).navigate(R.id.action_favoritesFragment_to_coinFragment)
                }
                CoinsViewModel.CurrentCoin.currentCoin = coin
            }
        }
    }
}