package com.example.micoin.ui.main.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.micoin.R
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import com.example.micoin.ui.main.adapter.CoinListAdapter

class CoinListFragment : Fragment(R.layout.coin_list_fragment) {
    lateinit var recyclerView: RecyclerView
    lateinit var viewModel: CoinsViewModel
    lateinit var adapter : CoinListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CoinsViewModel::class.java)
        adapter = CoinListAdapter(viewModel)

        viewModel.getCoinsApi()
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.adapter = adapter


        viewModel.coinsAPI.observe(viewLifecycleOwner, {
            adapter.setCoins(it)
        })

        //adapter.setCoins(viewModel.coinsAPI)

    }
}