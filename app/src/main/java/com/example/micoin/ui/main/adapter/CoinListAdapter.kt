package com.example.micoin.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.widget.Button
import android.content.Context
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import com.example.micoin.*
import com.example.micoin.data.model.Coin
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import com.squareup.picasso.Picasso

//- - - --  - - - -- Testing: List<Coin> ==> viewModel
class CoinListAdapter(val viewModel: CoinsViewModel?) : RecyclerView.Adapter<CoinListAdapter.CoinListViewHolder>() {
    private var coins = mutableListOf<Coin>()

    fun setCoins(coinList: List<Coin>) {
        coins = coinList.toMutableList()
        notifyDataSetChanged()
        //Log.i("TEST", "${coins.size}")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_coin_list, parent, false)
        return CoinListViewHolder(view)
    }

    override fun onBindViewHolder(holder: CoinListViewHolder, position: Int) {
        holder.bindData(coins[position])
    }

    override fun getItemCount(): Int {
        //coins = viewModel.coinsAPI.value!!.toMutableList()
        return coins.size
    }

    inner class CoinListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var coinName: TextView = itemView.findViewById(R.id.coinName)
        private var coinLogo: ImageView = itemView.findViewById(R.id.coinLogo)
        private var status: ImageView = itemView.findViewById(R.id.status)
        private var itemBackground: View = itemView.findViewById(R.id.item)
        val inflater: LayoutInflater = itemView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.coin_list_fragment, null)


        fun bindData(coin: Coin) {
            coinName.text = coin.name
            Picasso.get().load(coin.icon).resize(200, 200).centerCrop().into(coinLogo)
            if (coin.priceChange1w >= 0) {
                status.setImageResource(R.drawable.neon_arrow_up)
            } else {
                status.setImageResource(R.drawable.neon_arrow_down)
            }
            //coinLogo.background = ContextCompat.getDrawable(itemView.context, R.drawable.bitcoin)
            if (coin.rank % 2 == 0) {
                itemBackground.background = ContextCompat.getDrawable(itemView.context, R.color.topbar_background)
                coinName.setTextColor(ContextCompat.getColor(itemView.context, R.color.background_light_yellow))
            } else {
                itemBackground.background = ContextCompat.getDrawable(itemView.context, R.color.gray)
                coinName.setTextColor(ContextCompat.getColor(itemView.context, R.color.textLight))
            }

            itemBackground.setOnClickListener {
                (itemView.context as MainActivity).supportFragmentManager.commit() {
                    Navigation.findNavController(itemView).navigate(R.id.action_coinListFragment_to_coinFragment)
                }
                CoinsViewModel.CurrentCoin.currentCoin = coin
            }
        }

    }
}
