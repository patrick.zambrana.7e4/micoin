package com.example.micoin.ui.main.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.micoin.R
import com.example.micoin.data.model.Coin
import com.example.micoin.databinding.SearchFragmentBinding
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import com.squareup.picasso.Picasso

class SearchFragment : Fragment() {

    private lateinit var binding: SearchFragmentBinding
    val viewModel: CoinsViewModel by activityViewModels()
    private lateinit var coinFinded: Coin

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = SearchFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //viewModel = ViewModelProvider(this).get(CoinsViewModel::class.java)
        var coins : MutableList<Coin> = mutableListOf()
        viewModel.coinsAPI.observe(viewLifecycleOwner, {
            coins = it.toMutableList()
        })

        binding.searchImageButton.setOnClickListener {
            Log.i("TEST", "${coins}")
            //coins = viewModel.coinsAPI.value
            var coinName = binding.searchCoinInputText.text.toString()
            for (coin in coins!!) {
                if (coin.name.lowercase().contains(coinName.lowercase())) {
                    coinFinded = coin
                    break
                } else if (coinName == "") {
                    Toast.makeText(view.context, "No coin finded", Toast.LENGTH_LONG).show()
                }
            }
            loadCoin(view.context)
        }
    }

    private fun loadCoin(context: Context) {
        binding.coinFinded.background = ContextCompat.getDrawable(context, R.color.gray)
        Picasso.get().load(coinFinded.icon).resize(200, 200).into(binding.searchCoinLogo)
        binding.searchCoinName.text = coinFinded.name
        if (coinFinded.priceChange1w >= 0) {
            binding.searchStatus.setImageResource(R.drawable.neon_arrow_up)
        } else {
            binding.searchStatus.setImageResource(R.drawable.neon_arrow_down)
        }
        binding.coinFinded.setOnClickListener {
            CoinsViewModel.CurrentCoin.currentCoin = coinFinded
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_searchFragment_to_coinFragment)
        }
    }
}