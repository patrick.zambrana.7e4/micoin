package com.example.micoin.ui.main.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.micoin.R
import com.example.micoin.data.model.Coin
import com.example.micoin.data.model.CoinApplication
import com.example.micoin.data.model.Stat
import com.example.micoin.ui.main.adapter.StatListAdapter
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import com.squareup.picasso.Picasso
import java.util.concurrent.Executors

class CoinFragment : Fragment(R.layout.coin_stats_fragment) {

    private lateinit var coinLogo: ImageView
    private lateinit var coinName: TextView
    private lateinit var status: ImageView
    private lateinit var backBtn: ImageView
    private lateinit var addFavBtn: Button
    private val viewModel: CoinsViewModel by activityViewModels()
    private lateinit var recyclerView: RecyclerView
    lateinit var adapter : StatListAdapter
    private var statList = mutableListOf<Stat>()
    private lateinit var coin : Coin

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)
        coin = CoinsViewModel.CurrentCoin.currentCoin
        var roomList: List<Coin> = listOf()
        Executors.newSingleThreadExecutor().execute {
            roomList = CoinApplication.database.CoinDAO().getAllCoins().toList()
            for (c in roomList) {
                if (c.id == coin.id) {
                    coin = c
                    addFavBtn.background = ContextCompat.getDrawable(view!!.context, R.drawable.unfav_button)
                    addFavBtn.text = "Favorite"

                }
            }
            //Log.i("TEST", "${roomList}")
        }

        recyclerView = view.findViewById(R.id.recycler_view_stats)
        recyclerView.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.adapter = adapter
        Picasso.get().load(coin.icon).resize(300, 300).into(coinLogo)
        coinName.text = coin.name
        if (coin.priceChange1w >= 0) {
            status.setImageResource(R.drawable.neon_arrow_up)
        } else {
            status.setImageResource(R.drawable.neon_arrow_down)
        }

        setStatList()
        for (stat in statList) {
            if (stat.value == null) {
                stat.value = "No value found"
            }
        }
        adapter.setStats(statList)


        backBtn.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_coinFragment_to_coinListFragment)
        }
        addFavBtn.setOnClickListener {
            //Log.i("TEST", "${coin}")
            if (!coin.isFavorite) {
                addFav()
            } else {
                deleteFav()
            }
            //Log.i("TEST", "${coin.isFavorite}")
        }
    }

    private fun initViews(view: View) {
        coinLogo = view.findViewById(R.id.stats_coin_image)
        coinName = view.findViewById(R.id.stats_coin_name)
        status = view.findViewById(R.id.stats_arrow)
        backBtn = view.findViewById(R.id.back_to_home)
        addFavBtn = view.findViewById(R.id.addfav_button)
        recyclerView = view.findViewById(R.id.recycler_view_stats)
        adapter = StatListAdapter()
    }

    private fun addFav() {
        Executors.newSingleThreadExecutor().execute {
            addFavBtn.background = ContextCompat.getDrawable(view!!.context, R.drawable.unfav_button)
            addFavBtn.text = "Favorite"
            coin.isFavorite = true
            CoinApplication.database.CoinDAO().addCoin(coin)
        }
    }

    private fun deleteFav() {
        Executors.newSingleThreadExecutor().execute {
            addFavBtn.background = ContextCompat.getDrawable(view!!.context, R.drawable.tofav_button)
            addFavBtn.text = "Add to favorite"
            coin.isFavorite = false
            CoinApplication.database.CoinDAO().deleteCoin(coin)
        }
    }

    private fun setStatList() {
        statList.add(Stat("Price: ", String.format("%.2f", coin.price) + " EUR"))
        statList.add(Stat("Symbol: ", coin.symbol))
        statList.add(Stat("Rank: ", coin.rank.toString()))
        statList.add(Stat("Website: ", coin.websiteUrl))
        statList.add(Stat("Twitter: ", coin.twitterUrl))
        statList.add(Stat("Volume: ", coin.volume.toString()))
        //statList.add(Stat("Market Cap: ", coin.marketCap.toString()))
        statList.add(Stat("Price in BTC: ", String.format("%.4f", coin.priceBtc) + " EUR"))
        statList.add(Stat("Price changes (1 week): ", coin.priceChange1w.toString() + " EUR"))
        statList.add(Stat("Price changes (1 day): ", coin.priceChange1d.toString() + " EUR"))
        statList.add(Stat("Price  changes (1 hour): ", coin.priceChange1h.toString() + " EUR"))
        //statList.add(Stat(""))
    }

}