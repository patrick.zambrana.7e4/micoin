package com.example.micoin.ui.main.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.micoin.R
import com.example.micoin.data.model.Coin
import com.example.micoin.data.model.CoinApplication
import com.example.micoin.ui.main.adapter.CoinListAdapter
import com.example.micoin.ui.main.adapter.FavoriteAdapter
import com.example.micoin.ui.main.viewModel.CoinsViewModel
import java.util.concurrent.Executors

class FavoriteFragment : Fragment(R.layout.fragment_favorite) {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter : FavoriteAdapter
    val viewModel: CoinsViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = FavoriteAdapter()

        recyclerView = view.findViewById(R.id.recycler_view_favorites)
        recyclerView.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.adapter = adapter

        Executors.newSingleThreadExecutor().execute {
            var coinList : List<Coin>
            coinList = CoinApplication.database.CoinDAO().getAllCoins().toList()
            adapter.setCoins(coinList)
        }
    }
}