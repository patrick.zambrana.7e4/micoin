package com.example.micoin.ui.main.viewModel

import retrofit2.Call
import retrofit2.Callback
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.micoin.data.model.Coin
import com.example.micoin.data.model.CoinList
import com.example.micoin.data.api.ApiInterface
import com.example.micoin.data.api.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class CoinsViewModel : ViewModel() {

    private val apiInterface = ApiInterface.create()
    private val repository: Repository = Repository()
    var coinsAPI = MutableLiveData<List<Coin>>()

    object CurrentCoin {
        lateinit var currentCoin: Coin
    }

    init {
        getCoinsApi()
    }

    fun getCoinsApi() {
        val callCoins = apiInterface.getCoins()
        callCoins.enqueue(object: Callback<CoinList> {
            override fun onFailure(call: Call<CoinList>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<CoinList>, response: Response<CoinList?>) {
                if (response != null && response.isSuccessful) {
                    coinsAPI.value = (response.body()!!.coins)
                    //Log.i("test", "${coinsAPI.value}")
                }
            }
        })
    }
/*
    fun getCoin(id: Long) : Coin? {
        for (coin in coinsAPI.value!!) {
            if(coin.id.equals(id.toString()))
                return coin
        }
        return null
    }
*/
}